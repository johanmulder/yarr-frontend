<?php

namespace yarr\session;

/**
 * Class SessionHandlerImpl.
 * @author JH 
 */
Class SessionHandlerImpl implements SessionHandler
{
  public function __construct() {
    if (!session_id()) session_start();
  }
  
	/**
	 * Get the session id attached to this session.
	 * @return string
	 */
	public function getSessionId(){
		return session_id();
	}
	
	/**
	 * Get data for a given key in the session.
	 * @param string $key
	 * @return null|mixed Returns null if not found, the data otherwise.
	 */
	public function get($key){
		$data = null;
		if (isset($_SESSION[$key])){
			$data = $_SESSION[$key];
		}
		return $data;
	}
	
	/**
	 * Set data for a given key in the session.
	 * @param string $key
	 * @param mixed $data
	 * @return void
	 */
	public function set($key, $data){
		$_SESSION[$key] = $data;
	}
	
	/**
	 * Clear data for the given session key.
	 * @param string $key
	 * @return void
	 */
	public function clear($key){
		unset($_SESSION[$key]);
	}
	
	/**
	 * Destroy the complete session.
	 * @return void
	 */
	public function destroy(){
		session_destroy();
	}
}
