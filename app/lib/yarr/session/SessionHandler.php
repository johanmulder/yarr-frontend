<?php
namespace yarr\session;

/**
 * Interface defining a session handler.
 * @author Johan Mulder <johan@mulder.net>
 */
interface SessionHandler 
{
	/**
	 * Get the session id attached to this session.
	 * @return string
	 */
	public function getSessionId();
	
	/**
	 * Get data for a given key in the session.
	 * @param string $key
	 * @return null|mixed Returns null if not found, the data otherwise.
	 */
	public function get($key);
	
	/**
	 * Set data for a given key in the session.
	 * @param string $key
	 * @param mixed $data
	 * @return void
	 */
	public function set($key, $data);
	
	/**
	 * Clear data for the given session key.
	 * @param string $key
	 * @return void
	 */
	public function clear($key);
	
	/**
	 * Destroy the complete session.
	 * @return void
	 */
	public function destroy();
}
