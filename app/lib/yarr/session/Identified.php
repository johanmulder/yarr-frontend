<?php

namespace yarr\session;

/**
 * Class Identified, check if a security ticket is known in the session.
 * If it doesnt have a security ticket, the user isnt logged in.
 * @author JH 
 */
Class Identified
{
  private $session = null;
	
  public function __construct($session) {
		$this->session = $session;
  }
  
	/**
	 * This function checks if there a security ticket is known in the session.
	 * @return boolean status.
	 */
	public function getStatus(){
		if ($this->session->get("securityticket") === null){ 
			$status = false; // Security ticket absent.
		}
		
		else {
			$status = true; // Security ticket present. 
		}	
		
		return $status;
	}		
}
