<?php

namespace yarr\dao;

/**
 * Facade to all relevant DAO objects.
 * @author Johan Mulder
 *
 */
interface DAOFacade 
{
	/**
	 * Get the FeedDAO
	 * @return FeedDAO
	 */
	public function getFeedDAO();

	/**
	 * Get the FeedMapDAO
	 * @return FeedMapDAO
	 */
	public function getFeedMapDAO();
	
	/**
	 * Get the FeedSubscriptionDAO
	 * @return FeedSubscriptionDAO
	 */	
	public function getFeedSubscriptionDAO();
	
	/**
	 * Get the ItemReadDAO
	 * @return ItemReadDAO
	 */
	public function getItemReadDAO();
	
	/**
	 * Get the SettingDAO
	 * @return SettingDAO
	 */
	public function getSettingDAO();
	
	/**
	 * Get the UserDAO
	 * @return UserDAO
	 */
	 public function getUserDAO();
	
	/**
	 * Get the UserSettingsDAO
	 * @return UserSettingsDAO
	 */
	public function getUserSettingsDAO();
}
