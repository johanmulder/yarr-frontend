<?php
namespace yarr\dao;

use yarr\domain\FeedMap;

/**
 * FeedMap DAO.
 * @author Johan Mulder & Vincent Vogelesang
 */
interface FeedMapDAO 
{
	/**
	 * Get a Feed by remote_id.
	 * @param string $remoteId
	 * @return yarr\domain\Feed object
	 */
	public function getFeedByRemoteId($remoteId);
	
	/**
	 * Get a Feed by id.
	 * @param string $remoteId
	 * @return yarr\domain\Feed object
	 */
	public function getFeedById($id);
	
	/**
	 * Create a FeedMap
	 * @param FeedMap $feedMap
	 */
	public function create(FeedMap $feedMap);
	
	/**
	 * Update a FeedMap
	 * @param FeedMap $feedMap
	 */
	public function update(FeedMap $feedMap);
	
	/**
	 * Delete a FeedMap
	 * @param FeedMap $feedMap
	 */
	public function delete(FeedMap $feedMap);
}
