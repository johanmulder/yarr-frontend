<?php
namespace yarr\dao;

use yarr\domain\User;

/**
 * User DAO.
 * @author Johan Mulder <johan@mulder.net>
 */
interface UserDAO 
{
	/**
	 * Get a user by email.
	 * @param string $email
	 * @return yarr\domain\User
	 */
	public function getUserByEmail($email);
	
	/**
	 * Create a user
	 * @param User $User
	 */
	public function create(User $user);
	
	/**
	 * Update a user
	 * @param User $user
	 */
	public function update(User $user);
	
	/**
	 * Delete a user
	 * @param User $user
	 */
	public function delete(User $user);
	
	/**
	 * Make a User object from object
	 * @param object $object
	 * @return User object
	 */
	public function mapRow($object);
}
