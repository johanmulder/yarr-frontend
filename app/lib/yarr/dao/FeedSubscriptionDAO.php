<?php
namespace yarr\dao;

use yarr\domain\FeedSubscription;

/**
 * FeedSubscription DAO.
 * @author Johan Mulder & Vincent Vogelesang
 */
interface FeedSubscriptionDAO 
{
	/**
	 * Get a FeedSubscription by FeedSubscriptionname.
	 * @param string $FeedSubscriptionname
	 * @return yarr\domain\FeedSubscription
	 */
	public function getFeedSubscriptionByUserId($userId);
	
	/**
	 * Create a FeedSubscription
	 * @param FeedSubscription $FeedSubscription
	 */
	public function create(FeedSubscription $feedSubscription);
	
	/**
	 * Update a FeedSubscription
	 * @param FeedSubscription $FeedSubscription
	 */
	public function update(FeedSubscription $feedSubscription);
	
	/**
	 * Delete a FeedSubscription
	 * @param FeedSubscription $FeedSubscription
	 */
	public function delete(FeedSubscription $feedSubscription);
}
