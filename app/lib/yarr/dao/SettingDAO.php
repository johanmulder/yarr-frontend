<?php
namespace yarr\dao;

use yarr\domain\Setting;

/**
 * setting DAO.
 * @author Johan Mulder & Vincent Vogelesang
 */
interface SettingDAO 
{
	/**
	 * Get a setting by description.
	 * @param string $description
	 * @return yarr\domain\Setting object
	 */
	public function getSettingByDescription($description);
	
	/**
	 * Create a setting
	 * @param setting $setting
	 * @return PDO execute resultcode
	 */
	public function create(Setting $setting);
	
	/**
	 * Update a setting
	 * @param setting $setting
	 * @return PDO execute resultcode
	 */
	public function update(Setting $setting);
	
	/**
	 * Delete a setting
	 * @param setting $setting
	 * @return PDO execute resultcode
	 */
	public function delete(Setting $setting);
}
