<?php
namespace yarr\dao;

use yarr\domain\UserSettings;

/**
 * UserSettings DAO.
 * @author Johan Mulder & Vincent Vogelesang
 */
interface UserSettingsDAO 
{
	/**
	 * Get a UserSettings by user_id.
	 * @param string $settingId
	 * @return yarr\domain\UserSettings
	 */
	public function getUserSettingsByName($userId);
	
	/**
	 * Create a UserSettings
	 * @param UserSettings $userSettings
	 */
	public function create(UserSettings $userSettings);
	
	/**
	 * Update a UserSettings
	 * @param UserSettings $userSettings
	 */
	public function update(UserSettings $userSettings);
	
	/**
	 * Delete a UserSettings
	 * @param UserSettings $userSettings
	 */
	public function delete(UserSettings $userSettings);
}
