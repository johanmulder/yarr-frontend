<?php
namespace yarr\dao;

use yarr\domain\ItemRead;

/**
 * ItemRead DAO.
 * @author Johan Mulder & Vincent Vogelesang
 */
interface ItemReadDAO 
{
	/**
	 * Get a ItemRead by remoteId
	 * @param string $remoteId
	 * @return yarr\domain\ItemRead
	 */
	public function getItemReadByRemoteId($remoteId);
	
	/**
	 * Create a ItemRead
	 * @param ItemRead $itemRead
	 */
	public function create(ItemRead $itemRead);
	
	/**
	 * Update a ItemRead
	 * @param ItemRead $itemRead
	 */
	public function update(ItemRead $itemRead);
	
	/**
	 * Delete a ItemRead
	 * @param ItemRead $itemRead
	 */
	public function delete(ItemRead $itemRead);
}
