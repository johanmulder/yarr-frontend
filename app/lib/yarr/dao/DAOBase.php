<?php
namespace yarr\dao;

use yarr\Database ;

abstract class DAOBase
{
	/**
	 * @var \Database
	 */
	protected $database = null;

	public function __construct(Database $db)
	{
		$this->database = $db;
	}
}
