<?php
namespace yarr\dao;

use yarr\domain\Feed;

/**
 * Feed DAO.
 * @author Vincent Vogelesang
 */
interface FeedDAO 
{
	/**
	 * Get a Feed object by feed_id.
	 * @param string $feedId
	 * @return Feed object
	 */
	public function getFeed($feedId);
	
	/**
	 * Get a FeedItems by feed_id.
	 * @param string $feedId
	 * @param int number of items to return
	 * @param int offset itemno to start with
	 * @return Array of FeedItem objects
	 */
	public function getFeedItems($feedId, $max = 0, $offset = 0);
	
	/**
	 * Add a Feed
	 * @param string $url
	 * @return string $feedId
	 */
	public function addFeed($url);	
	
	/**
	 * Search for items containing seacrch string
	 * @param string search string
	 * @param array with feedId's to search
	 * @return an array with feedItems
	 */
	public function getSearchItems($searchString, $feedArray);
}
