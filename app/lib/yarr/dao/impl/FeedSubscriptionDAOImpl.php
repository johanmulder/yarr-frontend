<?php
namespace yarr\dao\impl;

use yarr\domain\FeedSubscription;
use yarr\dao\DAOBase;
use yarr\dao\FeedSubscriptionDAO;
use yarr\Database;

/**
 * User DAO.
 * @author Johan Mulder <johan@mulder.net> & Vincent Vogelesang
 */
Class FeedSubscriptionDAOImpl extends DAOBase implements FeedSubscriptionDAO
{
	const TABLE_FEEDSUBSCR = 'feed_subscription';
	
	/**
	 * Get a FeedSubscription by FeedSubscriptionname.
	 * @param string $userId
	 * @return yarr\domain\FeedSubscription
	 */
	public function getFeedSubscriptionByUserId($userId) 
	{
		$sth = $this->database->prepare('SELECT * FROM ' . self::TABLE_FEEDSUBSCR . ' WHERE user_id = :userId');
		$sth->bindValue(':userId', $userId);
		$sth->execute();
		return $sth->fetchAll(Database::FETCH_OBJ);
	}	
	
	/**
	 * Create a FeedSubscription
	 * @param FeedSubscription $feedSubscription
	 * @return PDO execute resultcode
	 */
	public function create(FeedSubscription $feedSubscription)
	{
		$sth = $this->database->prepare('INSERT INTO ' . self::TABLE_FEEDSUBSCR . '(feed_subscription_id, category_id, user_id, feed_id, name) VALUES (NULL, :categoryId, :userId, :feedId, :name)');
		$sth->bindValue(':userId', $feedSubscription->getUserId());
		$sth->bindValue(':feedId', $feedSubscription->getFeedId());
		$sth->bindValue(':categoryId', $feedSubscription->getCategoryId());
		$sth->bindValue(':name', $feedSubscription->getName());
		return $sth->execute();
	}
	
	/**
	 * Update a FeedSubscription
	 * @param FeedSubscription $feedSubscription
	 * @return PDO execute resultcode
	 */
	public function update(FeedSubscription $feedSubscription)
	{
		$sth = $this->database->prepare('UPDATE ' . self::TABLE_FEEDSUBSCR . ' SET user_id = :userId,  feed_id = :feedId WHERE feed_subscription_id = :feedSubscrId');
		$sth->bindValue(':feedSubscrId', $feedSubscription->getId());
		$sth->bindValue(':userId', $feedSubscription->getUserId());
		$sth->bindValue(':feedId', $feedSubscription->getFeedId());
		return $sth->execute();
	}
	
	/**
	 * Delete a FeedSubscription
	 * @param FeedSubscription $feedSubscription
	 * @return PDO execute resultcode
	 */
	public function delete(FeedSubscription $feedSubscription)
	{
		$sth = $this->database->prepare('DELETE FROM ' . self::TABLE_FEEDSUBSCR . ' WHERE feed_subscription_id = :feedSubscrId');
		$sth->bindValue(':feedSubscrId', $feedSubscription->getId());
		return $sth->execute();
	}
}
