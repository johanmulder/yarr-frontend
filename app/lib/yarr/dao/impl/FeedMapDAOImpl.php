<?php
namespace yarr\dao\impl;


use yarr\domain\FeedMap;
use yarr\dao\DAOBase;
use yarr\dao\FeedMapDAO;
use yarr\Database;

/**
 * FeedMap DAO.
 * @author Johan Mulder & Vincent Vogelesang
 */
Class FeedMapDAOImpl extends DAOBase implements FeedMapDAO
{
	
	const TABLE_FEED = 'feed';	
	
	/**
	 * Get a Feed by remote_id.
	 * @param string $remoteId
	 * @return yarr\domain\Feed object
	 */
	public function getFeedByRemoteId($remoteId) 
	{
		$sth = $this->database->prepare('SELECT * FROM ' . self::TABLE_FEED . ' WHERE remote_id = :remoteId');
		$sth->bindValue(':remoteId', $remoteId);
		$sth->execute();
		return $sth->fetchAll(Database::FETCH_OBJ);
	}

	/**
	 * Get a Feed by id.
	 * @param string $id
	 * @return yarr\domain\Feed object
	 */
	public function getFeedById($id)
	{
		$sth = $this->database->prepare('SELECT * FROM ' . self::TABLE_FEED . ' WHERE feed_id = :id');
		$sth->bindValue(':id', $id);
		$sth->execute();
		return $sth->fetchAll(Database::FETCH_OBJ);
	}
	
	/**
	 * Create a FeedMap
	 * @param FeedMap $feedMap
	 * @return PDO execute resultcode
	 */
	public function create(FeedMap $feedMap) 
	{
		$sth = $this->database->prepare('INSERT INTO ' . self::TABLE_FEED . '(feed_id, remote_id) VALUES (NULL, :remoteId)');
		$sth->bindValue(':remoteId', $feedMap->getRemoteId());
		return $sth->execute();
	}
	
	/**
	 * Update a FeedMap
	 * @param FeedMap $feedMap
	 * @return PDO execute resultcode
	 */
	public function update(FeedMap $feedMap)
	{
		$sth = $this->database->prepare('UPDATE ' . self::TABLE_FEED . ' SET remote_id = :remoteId WHERE feed_id = :id');
		$sth->bindValue(':id', $feedMap->getId());
		$sth->bindValue(':remoteId', $feedMap->getRemoteId());
		return $sth->execute();
	}
	
	/**
	 * Delete a FeedMap
	 * @param FeedMap $feedMap
	 * @return PDO execute resultcode
	 */
	public function delete(FeedMap $feedMap) 
	{
		$sth = $this->database->prepare('DELETE FROM ' . self::TABLE_FEED . ' WHERE feed_id = :id');
		$sth->bindValue(':id', $feedMap->getId());
		return $sth->execute();
	}
}
