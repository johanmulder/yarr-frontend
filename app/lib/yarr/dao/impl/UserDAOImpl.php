<?php
namespace yarr\dao\impl;

use yarr\domain\User;
use yarr\dao\DAOBase;
use yarr\dao\UserDAO;
use yarr\Database;

/**
 * User DAO.
 * @author Johan Mulder & Vincent Vogelesang & JH
 */
class UserDAOImpl extends DAOBase implements UserDAO
{
	
	const TABLE_USER = 'user';
	
	/**
	 * Get a user by email.
	 * @param string $email
	 * @return User object
	 */
	public function getUserByEmail($email)
	{
		$sth = $this->database->prepare('SELECT * FROM ' . self::TABLE_USER . ' WHERE email = :email');
		$sth->bindValue(':email', $email);
		$sth->execute();
		return $this->mapRow($sth->fetch(Database::FETCH_OBJ));
	}
	
	/**
	 * Create a user
	 * @param User $User
	 * @return PDO execute resultcode
	 */
	public function create(User $user) 
	{
		$sth = $this->database->prepare('INSERT INTO ' . self::TABLE_USER . '(user_id, password, email, status) VALUES (NULL, :password, :email, :status)');
		$sth->bindValue(':email', $user->getEmail());
		$sth->bindValue(':password', $user->getPassword());
		$sth->bindValue(':status', $user->getStatus());
		return $sth->execute();		
	}
		
	/**
	 * Update a user
	 * @param User $user
	 * @return PDO execute resultcode
	 */
	public function update(User $user) {
		$sth = $this->database->prepare('UPDATE ' . self::TABLE_USER . ' SET email = :email, password = :password, status = :status WHERE user_id = :id');
		$sth->bindValue(':id', $user->getId());
		$sth->bindValue(':email', $user->getEmail());
		$sth->bindValue(':password', $user->getPassword());
		$sth->bindValue(':status', $user->getStatus());				
		return $sth->execute();				
	}
	
	/**
	 * Delete a user
	 * @param User $user
	 * @return PDO execute resultcode
	 */
	public function delete(User $user) 
	{
		$sth = $this->database->prepare('DELETE FROM ' . self::TABLE_USER . ' WHERE user_id = :id');
		$sth->bindValue(':id', $user->getId());
		return $sth->execute();
	}	
	
	/**
	 * Make a User object from object
	 * @param object $object
	 * @return User object
	 */
	public function mapRow($object)
	{
		$user = new User();
		if ($object) {	 // If nothing given to $object then object is false, all other true.
			$user->setId($object->user_id);
			$user->setEmail($object->email);
			$user->setPassword($object->password);
			$user->setStatus($object->status);
		}
		return $user;
	}
}
