<?php
namespace yarr\dao\impl;

use yarr\domain\UserSettings;
use yarr\dao\UserSettingsDAO;
use yarr\dao\DAOBase;
use yarr\Database;

/**
 * UserSettings DAO.
 * @author Johan Mulder & Vincent Vogelesang
 */
class UserSettingsDAOImpl extends DAOBase implements UserSettingsDAO 
{
	const TABLE_USERSETTINGS = 'user_settings';
	
	/**
	 * Get a UserSettings by user_id.
	 * @param string $settingId
	 * @return yarr\domain\UserSettings object
	 */
	public function getUserSettingsByName($userId) 
	{
		$sth = $this->database->prepare('SELECT * FROM ' . self::TABLE_USERSETTINGS . ' WHERE user_id = :userId');
		$sth->bindValue(':userId', $userId);
		$sth->execute();
		return $sth->fetchAll(Database::FETCH_OBJ);
	}
	
	/**
	 * Create a UserSettings
	 * @param UserSettings $userSettings
	 * @return PDO execute resultcode
	 */
	public function create(UserSettings $userSettings)
	{
		$sth = $this->database->prepare('INSERT INTO ' . self::TABLE_USERSETTINGS . '(user_id, value, setting) VALUES (:userId, :value, :setting)');
		$sth->bindValue(':userId', $userSettings->getId());
		$sth->bindValue(':value', $userSettings->getValue());
		$sth->bindValue(':setting', $userSettings->getSetting());
		return $sth->execute();
	}
	
	/**
	 * Update a UserSettings
	 * @param UserSettings $userSettings
	 * @return PDO execute resultcode
	 */
	public function update(UserSettings $userSettings)
	{
		$sth = $this->database->prepare('UPDATE ' . self::TABLE_USERSETTINGS . ' SET user_id = :userId, value = :value WHERE setting = :setting');
		$sth->bindValue(':userId', $userSettings->getId());
		$sth->bindValue(':value', $userSettings->getValue());
		$sth->bindValue(':setting', $userSettings->getSetting());
		return $sth->execute();
	}
	
	/**
	 * Delete a UserSettings
	 * @param UserSettings $userSettings
	 * @return PDO execute resultcode
	 */
	public function delete(UserSettings $userSettings)
	{
		$sth = $this->database->prepare('DELETE FROM ' . self::TABLE_USERSETTINGS . ' WHERE user_id = :id');
		$sth->bindValue(':id', $userSettings->getId());
		return $sth->execute();
	}
}
