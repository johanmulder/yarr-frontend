<?php

namespace yarr\dao\impl;

use yarr\dao\DAOFacade;
use yarr\Database;
use yarr\dao\impl\FeedDAOImpl;
use yarr\dao\impl\FeedMapDAOImpl;
use yarr\dao\impl\FeedSubscriptionDAOImpl;
use yarr\dao\impl\ItemReadDAOImpl;
use yarr\dao\impl\SettingDAOImpl;
use yarr\dao\impl\UserDAOImpl;
use yarr\dao\impl\UserSettingsDAOImpl;


/**
 * Facade to all relevant DAO objects.
 * @author Johan Mulder & Vincent Vogelesang & JH
 *
 */
class DAOFacadeImpl implements DAOFacade  
{
	/**
	 * Database object.
	 * @var Database
	 */
	private $database = null;
	private $feedDAO = null;
	private $feedMapDAO = null;
	private	$feedSubscriptionDAO = null;
	private	$itemReadDAO = null;
	private	$settingDAO = null;
	private $userDAO = null;
	private	$userSettingsDAO = null;
	
	/**
	 * Class constructor.
	 * @param Database $pdo, 
	 * @param string base REST url
	 */
	public function __construct(Database $db, $restUrl) {
		$this->database = $db;
		$this->feedDAO = new FeedDAOImpl($restUrl);
		$this->feedMapDAO = new FeedMapDAOImpl($this->database);
		$this->feedSubscriptionDAO = new FeedSubscriptionDAOImpl($this->database);
		$this->itemReadDAO = new ItemReadDAOImpl($this->database);
		$this->settingDAO = new SettingDAOImpl($this->database);
		$this->userDAO = new UserDAOImpl($this->database);
		$this->userSettingsDAO = new UserSettingsDAOImpl($this->database);	
	}	
	
	/**
	 * Get the FeedDAO
	 * @return FeedDAO
	 */
	public function getFeedDAO() {
		return $this->feedDAO;
	}
	
	/**
	 * Get the FeedItemDAO
	 * @return FeedItemDAO
	 */
	public function getFeedItemDAO() {
		return $this->feedItemDAO;
	}
	
	/**
	 * Get the FeedMapDAO
	 * @return FeedMapDAO
	 */
	public function getFeedMapDAO() {
		return $this->feedMapDAO;
	}
	
	/**
	 * Get the FeedSubscriptionDAO
	 * @return FeedSubscriptionDAO
	 */	
	public function getFeedSubscriptionDAO() {
		return $this->feedSubscriptionDAO;	
	}
	
	/**
	 * Get the ItemReadDAO
	 * @return ItemReadDAO
	 */
	public function getItemReadDAO() {
		return $this->itemReadDAO;	
	}
	
	/**
	 * Get the SettingDAO
	 * @return SettingDAO
	 */
	public function getSettingDAO() {
		return $this->settingDAO;	
	}
	
	/**
	 * Get the UserDAO
	 * @return UserDAO
	 */
	 public function getUserDAO() {
		return $this->userDAO;
	}
	
	/**
	 * Get the UserSettingsDAO
	 * @return UserSettingsDAO
	 */
	public function getUserSettingsDAO() {
		return $this->userSettingsDAO;	
	}	
}