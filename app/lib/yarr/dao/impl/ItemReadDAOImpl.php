<?php
namespace yarr\dao\impl;

use yarr\dao\ItemReadDAO;
use yarr\domain\ItemRead;
use yarr\dao\DAOBase;
use yarr\Database;

/**
 * ItemRead DAO.
 * @author Johan Mulder <johan@mulder.net> & Vincent Vogelesang
 */
class ItemReadDAOImpl extends DAOBase implements ItemReadDAO 
{
	
	const TABLE_ITEMREAD = 'item_read';
	
	/**
	 * Get a ItemRead by remoteId.
	 * @param string $remoteId
	 * @return yarr\domain\ItemRead object
	 */
	public function getItemReadByRemoteId($remoteId)
	{
		$sth = $this->database->prepare('SELECT * FROM ' . self::TABLE_ITEMREAD . ' WHERE remote_id = :remoteId');
		$sth->bindValue(':remoteId', $remoteId);
		$sth->execute();
		return $sth->fetchAll(Database::FETCH_OBJ);
	}
	
	/**
	 * Create a ItemRead
	 * @param ItemRead $itemRead
	 * @return PDO execute resultcode
	 */
	public function create(ItemRead $itemRead)
	{
		$sth = $this->database->prepare('INSERT INTO ' . self::TABLE_ITEMREAD . '(item_read_id, feed_id, user_id, remote_id) VALUES (NULL, :feedId, :userId, :remoteId)');
		$sth->bindValue(':feedId', $itemRead->getFeedId());
		$sth->bindValue(':userId', $itemRead->getUserId());
		$sth->bindValue(':remoteId', $itemRead->getRemoteId());
		return $sth->execute();
	}
	
	/**
	 * Update a ItemRead
	 * @param ItemRead $itemRead
	 * @return PDO execute resultcode
	 */
	public function update(ItemRead $itemRead) 
	{		
		$sth = $this->database->prepare('UPDATE ' . self::TABLE_ITEMREAD . ' SET feed_id = :feedId, user_id = :userId, remote_id = :remoteId WHERE item_read_id = :id');
		$sth->bindValue(':id', $itemRead->getId());
		$sth->bindValue(':feedId', $itemRead->getFeedId());
		$sth->bindValue(':userId', $itemRead->getUserId());
		$sth->bindValue(':remoteId', $itemRead->getRemoteId());
		return $sth->execute();
	}
	
	/**
	 * Delete a ItemRead
	 * @param ItemRead $itemRead
	 * @return PDO execute resultcode
	 */
	public function delete(ItemRead $itemRead)
	{
		$sth = $this->database->prepare('DELETE FROM ' . self::TABLE_ITEMREAD . ' WHERE item_read_id = :id');
		$sth->bindValue(':id', $itemRead->getId());
		return $sth->execute();
	}
}
