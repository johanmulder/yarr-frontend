<?php
namespace yarr\dao\impl;

use yarr\domain\Setting;
use yarr\dao\DAOBase;
use yarr\dao\SettingDAO;
use yarr\Database;

/**
 * setting DAO.
 * @author Johan Mulder & Vincent Vogelesang
 */
class SettingDAOImpl extends DAOBase implements SettingDAO
{
	
	const TABLE_SETTING = 'setting';
	
	/**
	 * Get a setting by description.
	 * @param string $description
	 * @return yarr\domain\Setting object
	 */
	public function getSettingByDescription($description) 
	{
		$sth = $this->database->prepare('SELECT * FROM ' . self::TABLE_SETTING . ' WHERE description = :description');
		$sth->bindValue(':description', $description);
		$sth->execute();
		return $sth->fetchAll(Database::FETCH_OBJ);
	}
	
	/**
	 * Create a setting
	 * @param setting $setting
	 * @return PDO execute resultcode
	 */
	public function create(Setting $setting) 
	{
		$sth = $this->database->prepare('INSERT INTO ' . self::TABLE_SETTING . '(setting, description) VALUES (NULL, :description)');
		$sth->bindValue(':description', $setting->getDescription());
		return $sth->execute();
	}
	
	/**
	 * Update a setting
	 * @param setting $setting
	 * @return PDO execute resultcode
	 */
	public function update(Setting $setting)
	{
		$sth = $this->database->prepare('UPDATE ' . self::TABLE_SETTING . ' SET description = :description WHERE setting = :setting');
		$sth->bindValue(':setting', $setting->getSetting());
		$sth->bindValue(':description', $setting->getDescription());
		return $sth->execute();
	}
	
	/**
	 * Delete a setting
	 * @param setting $setting
	 * @return PDO execute resultcode
	 */
	public function delete(Setting $setting)
	{
		$sth = $this->database->prepare('DELETE FROM ' . self::TABLE_SETTING . ' WHERE setting = :setting');
		$sth->bindValue(':setting', $setting->getSetting());
		return $sth->execute();
	}
}
