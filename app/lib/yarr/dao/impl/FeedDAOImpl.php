<?php
namespace yarr\dao\impl;

use yarr\domain\Feed;
use yarr\domain\FeedItem;
use yarr\dao\FeedDAO;

/**
 * Feed DAO.
 * @author Vincent Vogelesang
 */
class FeedDAOImpl implements FeedDAO 
{
	/**
	 * @var rest url
	 */
	private $restUrl = null;
	private $dateFormat = '%Y-%m-%d %H:%M:%S';
	
	public function __construct($restUrl)
	{
		$this->restUrl = $restUrl;	
	}	
	
	/**
	 * Get a Feed object by feed_id.
	 * @param string $feedId
	 * @return Feed object
	 */
	public function getFeed($feedId)
	{
		$feed = null;
		
		// Get the data from feedId		
		$res = curl_init($this->restUrl . 'feeds/' . $feedId); // The url to get data from. restUrl is read form config.ini
		curl_setopt($res, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($res, CURLOPT_FAILONERROR, true); // Fail silently without garbling the page layout
		$data = curl_exec($res);
		
		// Get status code
		$status_code = curl_getinfo($res, CURLINFO_HTTP_CODE);
		curl_close($res);
		if ($status_code < 400) 
		{
			$feed = $this->mapFeed(json_decode($data)); // Return new Feed item from json stdclass object  		
		}
		return $feed;
	}	
	
	/**
	 * Get a FeedItems by feed_id.
	 * @param string $feedId
	 * @param int number of items to return
	 * @param int offset itemno to start with
	 * @return Array of FeedItem objects
	 */
	public function getFeedItems($feedId, $max = 0, $offset = 0)
	{
		$feedItemArray = array();
		
		// Get the data from feedId
		$res = curl_init($this->restUrl . 'feeds/feed_items/' . $feedId . '?max=' . $max . '&offset=' . $offset);
		curl_setopt($res, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($res, CURLOPT_FAILONERROR, true); // Fail silently without garbling the page layout
		$data = curl_exec($res);
		
		// Get status code
		$status_code = curl_getinfo($res, CURLINFO_HTTP_CODE);
		curl_close($res);		
		
		if ($status_code < 400) 
		{		
			//Parse data into array
			$array = json_decode($data); // temp array to hold the std object data
			foreach($array as $object) {
				array_push($feedItemArray, $this->mapFeedItem($object)); // Remap objects to FeedItems, add to Array
			}
		}				
		return $feedItemArray;
	}
	
	/**
	 * Search for items containing seacrch string
	 * @param string search string
	 * @param array with feedId's to search
	 * @return an array with feedItems
	 */
	public function getSearchItems($searchString, $feedArray)
	{
		$feedItemArray = array();
		
		// Turn the feedArray into a string of feedId's		
		$feeds = "?";
		foreach ($feedArray as $feedId)
		{
			$feeds = $feeds . '&feed=' . $feedId; 
		}
		
		// Get the data from feedId
		$res = curl_init($this->restUrl . 'feeds/feed_items/search/' . $searchString . $feeds);
		curl_setopt($res, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($res, CURLOPT_FAILONERROR, true); // Fail silently without garbling the page layout
		$data = curl_exec($res);
		
		// Get status code
		$status_code = curl_getinfo($res, CURLINFO_HTTP_CODE);
		curl_close($res);
		
		if ($status_code < 400)
		{
			//Parse data into array
			$array = json_decode($data); // temp array to hold the std object data
			foreach($array as $object) {
				array_push($feedItemArray, $this->mapFeedItem($object)); // Remap objects to FeedItems, add to Array
			}
		}
		return $feedItemArray;
	}
	
	/**
	 * Add a Feed
	 * @param string $url
	 * @return string $feedId
	 */
	public function addFeed($url) 
	{
		$remoteId = null;
		// Push data upstream
		$res = curl_init($this->restUrl . 'feeds/add/'); // De url om ADD te kunnen doen. restUrl bevat de basis url (http://blackraven.eu/rest/)
		curl_setopt($res, CURLOPT_POSTFIELDS, $url);
		curl_setopt($res, CURLOPT_POST, true);
		curl_setopt($res, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($res, CURLOPT_FAILONERROR, true);
		$data = curl_exec($res);
				
		// Get status code
		$status_code = curl_getinfo($res, CURLINFO_HTTP_CODE);
		curl_close($res);
		if ($status_code < 400) 
		{
			$remoteId = json_decode($data)->id;			
		}
		return $remoteId;
	}
	
	/**
	 * Make a Feed object from object
	 * @param object $object
	 * @return Feed object
	 */
	private function mapFeed($object)
	{
		$feed = new Feed();
		if (!is_null($object)) {
			$feed->setId($object->id);
			$feed->setTitle($object->title);
			$feed->setDescription($object->description);
			$feed->setLanguage($object->language);
			$feed->setLastUpdate(strftime($this->dateFormat, $object->lastUpdated / 1000));
			$feed->setPublishDate(strftime($this->dateFormat, $object->publishDate / 1000));
			$feed->setType($object->type);
			$feed->setUrl($object->url);
			$feed->setTtl($object->ttl);			
			$feed->setFeedUrl($object->feedUrl);
			$feed->setIcon($object->icon);
			$feed->setNumFeedItems($object->numFeedItems);
		}		
		return $feed;
	}
	
	/**
	 * Make a FeedItem object from object
	 * @param object $object
	 * @return FeedItem object
	 */
	private function mapFeedItem($object)
	{
		$feedItem = new FeedItem();
		if (!is_null($object)) {
			$feedItem->setId($object->id);
			$feedItem->setFeedId($object->feedId);
			$feedItem->setTitle($object->title);
			$feedItem->setGuid($object->guid);
			$feedItem->setUrl($object->url);
			$feedItem->setLastUpdate(strftime($this->dateFormat, $object->lastUpdated / 1000));
			$feedItem->setPubDate(strftime($this->dateFormat, $object->publishDate / 1000));
			$feedItem->setName($object->author->name);
			$feedItem->setEmail($object->author->email);
			if (isset ($object->description)) {
				$feedItem->setContents($object->description);
			}
			else if (isset ($object->contents)) {
				$feedItem->setContents($object->contents);
			}			
		}
		return $feedItem;
	}
}

