<?php

namespace yarr;

use yarr\dao\impl\DAOFacadeImpl;
use yarr\controller\LoginController;
use yarr\controller\RegisterController;
use yarr\controller\YarrController;
use yarr\session\Identified;

error_reporting(E_ALL);

/**
 * The main application class.
 * @author Johan Mulder & JH
 */
class Application 
{
	/**
	 * Configuration object.
	 * @var Configuration
	 */
	private $config = null;

	/**
	 * @var Database
	 */
	private $database = null;
	private $dbOwner = true;
	private $DAOFacade = null;
	
	/**
	 * @var session
	 */
	private $session = null;
	
	/**
	 * Class constructor
	 * @param Configuration $config Configuration data.
	 * @param PDO $pdo Optional PDO object to the database.
	 */
	public function __construct(Configuration $config, Database $db = null)
	{
		if (!is_null($config))
			$this->config = $config;
		if (is_null($db))
			// Connect to database.
			$this->database = new Database($config);
		else
		{
			// If the PDO is given, we're not the owner and we won't handle disconnection.
			$this->database = $db;
			$this->dbOwner = false;
		}
	}

	/**
	 * Class destructor.
	 */
	public function __destruct()
	{
		// Disconnect only if we're the owner of the PDO object.
		if ($this->dbOwner)
			unset($this->database);
	}
	
	/**
	 * Start running the application.
	 */
	public function run()
	{
		//Make a new session
		$this->startSession();
		
		$template_data = null;
		
		// Get the current action.
		$action = $this->getCurrentAction();
		
		// Initialize the controller class.
		$controller = $this->getCurrentController($this->database);
		$invoked = false;
		$reflec = new \ReflectionClass($controller);
		
		// Look for public methods in the Controller class.
		$methods = $reflec->getMethods(\ReflectionMethod::IS_PUBLIC);
		foreach ($methods as $method)
		// If the method name is "action$action" (which is defined in the current action),
		// invoke it.
		if (strcasecmp($method->name, 'action' . $action) == 0)
		{
			$template_data = $method->invoke($controller);
			$invoked = true;
			break;
		}
		
		// If no controller method was invoked, execute the default action.
		if (!$invoked)
		{
			$action = 'home';
			$template_data = $controller->actionHome();
		}
	
		$this->displayTemplate($this->stripControllerName($controller), $template_data);
	}
	
	/**
	 * Display a given template with the template data.
	 * @param string $template The template base name in the template dir.
	 * @param array $template_data Data to pass to the template.
	 */
	private function displayTemplate($template, $template_data)
	{
		// Include config in the template.
		$config = $this->config;
		// Content type with utf8 charset.
		header('Content-type: text/html; charset=utf-8');
		include(__DIR__ . '/template/' . $template . '.tpl.php');
		
	}
	
	/**
	 * This function starts a new session and database connection.
	 */	
	private function startSession() {
		//Begin a new session
		$this->session = new \yarr\session\SessionHandlerImpl();
						
		//Connection with the databases.
		$this->DAOFacade = new DAOFacadeImpl($this->database, $this->config->getRestUrl());
	}		

	/**
	 * This function determines the current action. No action
	 * means the defaultaction is used.
	 */
	private function getCurrentAction()
	{
		if (isset($_POST['action']) == true){
			return $_POST['action'];
		}
		else{
			return $this->config->getDefaultAction();
		}
	}
	
	/**
	 * This function determines the current controller. No controller
	 * means the defaultcontroller is used.
	 */
	private function getCurrentController(Database $db)
	{
		$identified = new Identified($this->session); // Object that knowns if a person is logged in, based on the presence of security ticket in session.
		
		if (isset($_SERVER['PATH_INFO']) == true) {
			if ($_SERVER['PATH_INFO'] === '/login') {
				return new LoginController($db, $this->DAOFacade, $this->session);
			}
			if ($_SERVER['PATH_INFO'] === '/register') {
				return new RegisterController($db, $this->DAOFacade, $this->session);
			}			
			if ($_SERVER['PATH_INFO'] === '/yarr' && $identified->getStatus()) {
					return new YarrController($db, $this->DAOFacade, $this->session);				
			}			
		}
		return new LoginController($db, $this->DAOFacade, $this->session);
							
	}	
	
	/**
	 * Strips 'controller' and namespace from controller object
	 *
	 * @param the controller object
	 * @return a string with controller template identifier
	 */
	private function stripControllerName($object)
	{
		$array = explode('\\',get_class($object));
		return strtolower(str_replace("Controller", "", $array[2]));
	
	}
}
