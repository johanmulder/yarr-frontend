<?php
namespace yarr\domain;

class Setting 
{
	private $setting;
	private $description;	
	// Whatever other field is needed.

	/**
	 * @return the $setting
	 */
	public function getSetting() 
	{
		return $this->setting;
	}

	/**
	 * @param field_type $setting
	 */
	public function setSetting($setting) 
	{
		$this->setting = $setting;
	}

	/**
	 * @return the $description
	 */
	public function getDescription() 
	{
		return $this->description;
	}

	/**
	 * @param field_type $description
	 */
	public function setDescription($description) 
	{
		$this->description = $description;
	}
}
