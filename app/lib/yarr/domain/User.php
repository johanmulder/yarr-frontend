<?php
namespace yarr\domain;

class User 
{
	private $id;
	private $email;
	private $password;
	private $status;
	// Whatever other field is needed.

	/**
	 * @return the $id
	 */
	public function getId() 
	{
		return $this->id;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) 
	{
		$this->id = $id;
	}

	/**
	 * @return the $email
	 */
	public function getEmail() 
	{
		return $this->email;
	}

	/**
	 * @param field_type $email
	 */
	public function setEmail($email) 
	{
		$this->email = $email;
	}

	/**
	 * @return the $password
	 */
	public function getPassword() 
	{
		return $this->password;
	}

	/**
	 * @param field_type $password
	 */
	public function setPassword($password) 
	{
		$this->password = $password;
	}
	
	/**
	 * @return the $status
	 */
	public function getStatus()
	{
		return $this->status;
	}
	
	/**
	 * @param field_type $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}
}
