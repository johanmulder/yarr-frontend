<?php
namespace yarr\domain;

class FeedItem
{
	// Object property fields.	
	private $id;
	private $feedId;
	private $title;
	private $description;
	private $contents;
	private $url;
	private $guid;
	private $pubDate;
	private $lastUpdate;
	private $name;
	private $email;
	
	// Getters and setters
	/**
	 * @return the id
	 */
	public function getId() 
	{
		return $this->id;
	}
	
	/**
	 * @return the FeedId
	 */
	public function getFeedId()
	{
		return $this->feedId;
	}

	/**
	 * @return the title
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	/**
	 * @return the description
	 */
	public function getDescription()
	{
		return $this->description;
	}
	
	/**
	 * @return the language
	 */
	public function getContents()
	{
		return $this->contents;
	}
	
	/**
	 * @return the url
	 */
	public function getUrl()
	{
		return $this->url;
	}
	
	/**
	 * @return the guid
	 */
	public function getGuid()
	{	
		return $this->guid;
	}
	
	/**
	 * @return the publication date
	 */
	public function getPubDate()
	{
		return $this->pubDate;
	}
		
	/**
	 * @return the last update
	 */
	public function getLastUpdate()
	{
		return $this->lastUpdate;
	}
	
	/**
	 * @return the author name
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * @return the author email
	 */
	public function getEmail()
	{
		return $this->email;
	}
	
	/**
	 * @param the id
	 */
	public function setId($id) 
	{
		$this->id = $id;
	}

	/**
	 * @param the feedId
	 */
	public function setFeedId($feedId)
	{
		$this->feedId = $feedId;
	}
	
	/**
	 * @param the title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * @param the description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}
	
	/**
	 * @param the contents
	 */
	public function setContents($contents)
	{
		$this->contents = $contents;
	}
	

	/**
	 * @param the url
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}
	
	/**
	 * @param the guid
	 */
	public function setGuid($guid)
	{
		$this->guid = $guid;
	}
	
	/**
	 * @param the pubDate
	 */
	public function setPubDate($pubDate)
	{
		$this->pubDate = $pubDate;
	}
	
	/**
	 * @param the last update
	 */
	public function setLastUpdate($lastUpdate)
	{
		$this->lastUpdate = $lastUpdate;
	}
	
	/**
	 * @param the author name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}
	
	/**
	 * @param the author email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}
}
