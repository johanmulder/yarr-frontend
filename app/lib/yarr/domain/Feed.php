<?php
namespace yarr\domain;

class Feed
{
	// Object property fields.	
	private $id;
	private $title;
	private $description;
	private $language;
	private $lastUpdate;
	private $publishDate;
	private $type;
	private $url;
	private $ttl;
	private $feedUrl;
	private $icon;
	private $numFeedItems;
	
	// Getters and setters
	/**
	 * @return the id
	 */
	public function getId() 
	{
		return $this->id;
	}

	/**
	 * @return the title
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	/**
	 * @return the description
	 */
	public function getDescription()
	{
		return $this->description;
	}
	
	/**
	 * @return the language
	 */
	public function getLanguage()
	{
		return $this->language;
	}
	
	/**
	 * @return the last update
	 */
	public function getLastUpdate()
	{
		return $this->lastUpdate;
	}
	
	/**
	 * @return the publishdate
	 */
	public function getPublishDate()
	{
		return $this->publishDate;
	}
	
	/**
	 * @return the type
	 */
	public function getType()
	{
		return $this->type;
	}
	
	/**
	 * @return the url
	 */
	public function getUrl()
	{
		return $this->url;
	}
	
	/**
	 * @return the ttl
	 */
	public function getTtl()
	{
		return $this->ttl;
	}
	
	/**
	 * @return the feed_url
	 */
	public function getFeedUrl()
	{
		return $this->feedUrl;
	}
	
	/**
	 * @return the icon
	 */
	public function getIcon()
	{
		return $this->icon;
	}
	
	/**
	 * @return the number of feed items
	 */
	public function getNumFeedItems()
	{
		return $this->numFeedItems;
	}
	
	/**
	 * @param the id
	 */
	public function setId($id) 
	{
		$this->id = $id;
	}	
	
	/**
	 * @param the title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * @param the description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}
	
	/**
	 * @param the language
	 */
	public function setLanguage($language)
	{
		$this->language = $language;
	}
	
	/**
	 * @param the last update
	 */
	public function setLastUpdate($lastUpdate)
	{
		$this->lastupdate = $lastUpdate;
	}
	
	/**
	 * @param the publish update
	 */
	public function setPublishDate($publishDate)
	{
		$this->publishDate = $publishDate;
	}
	
	/**
	 * @param the type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}
	
	/**
	 * @param the url
	 */
	public function setUrl($url)
	{
		 $this->url = $url;
	}
	
	/**
	 * @param the ttl
	 */
	public function setTtl($ttl)
	{
		$this->ttl = $ttl;
	}
	
	/**
	 * @param the feed_url
	 */
	public function setFeedUrl($feedUrl)
	{
		$this->feedUrl = $feedUrl;
	}
	
	/**
	 * @param the icon
	 */
	public function setIcon($icon)
	{
		$this->icon = $icon;
	}
	
	/**
	 * @param the number of items in the feed
	 */
	public function setNumFeedItems($numFeedItems)
	{
		$this->numFeedItems = $numFeedItems;
	}
}
