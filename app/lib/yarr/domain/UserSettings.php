<?php
namespace yarr\domain;

class UserSettings 
{
	private $id;
	private $value;
	private $setting;
	// Whatever other field is needed.

	/**
	 * @return the $id
	 */
	public function getId() 
	{
		return $this->id;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) 
	{
		$this->id = $id;
	}

	/**
	 * @return the $value
	 */
	public function getValue() 
	{
		return $this->value;
	}

	/**
	 * @param field_type $value
	 */
	public function setValue($value) 
	{
		$this->value = $value;
	}

	/**
	 * @return the $setting
	 */
	public function getSetting() 
	{
		return $this->setting;
	}

	/**
	 * @param field_type $setting
	 */
	public function setSetting($setting) 
	{
		$this->setting = $setting;
	}
}
