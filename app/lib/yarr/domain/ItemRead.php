<?php
namespace yarr\domain;

class ItemRead 
{
	private $id;
	private $feedId;
	private $userId;
	private $remoteId;
	// Whatever other field is needed.

	/**
	 * @return the $id
	 */
	public function getId() 
	{
		return $this->id;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) 
	{
		$this->id = $id;
	}

	/**
	 * @return the $feedId
	 */
	public function getFeedId() 
	{
		return $this->feedId;
	}

	/**
	 * @param field_type $feedId
	 */
	public function setFeedId($feedId) 
	{
		$this->feedId = $feedId;
	}

	/**
	 * @return the $userId
	 */
	public function getUserId() 
	{
		return $this->userId;
	}

	/**
	 * @param field_type $userId
	 */
	public function setUserId($userId) 
	{
		$this->userId = $userId;
	}
	
	/**
	 * @return the $remoteId
	 */
	public function getRemoteId()
	{
		return $this->remoteId;
	}
	
	/**
	 * @param field_type $remoteId
	 */
	public function setRemoteId($remoteId)
	{
		$this->remoteId = $remoteId;
	}
}
