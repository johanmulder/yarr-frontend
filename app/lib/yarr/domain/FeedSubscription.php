<?php
namespace yarr\domain;

class FeedSubscription 
{
	private $id;
	private $categoryId;
	private $userId;
	private $feedId;
	private $name;
	// Whatever other field is needed.

	/**
	 * @return the $id
	 */
	public function getId() 
	{
		return $this->id;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) 
	{
		$this->id = $id;
	}

	/**
	 * @return the $userId
	 */
	public function getUserId() 
	{
		return $this->userId;
	}

	/**
	 * @param field_type $userId
	 */
	public function setUserId($userId) 
	{
		$this->userId = $userId;
	}
	
	/**
	 * @return the $feedId
	 */
	public function getFeedId()
	{
		return $this->feedId;
	}
	
	/**
	 * @param field_type $feedId
	 */
	public function setFeedId($feedId)
	{
		$this->feedId = $feedId;
	}
	
	/**
	 * @return the $categoryId
	 */
	public function getCategoryId()
	{
		return $this->categoryId;
	}
	
	/**
	 * @param field_type $categoryId
	 */
	public function setCategoryId($categoryId)
	{
		$this->categoryId = $categoryId;
	}
	
	/**
	 * @return the $name
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * @param field_type $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}
}
