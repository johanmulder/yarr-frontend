<?php
namespace yarr\domain;

class Author
{
	// Object property fields.	
	private $name;
	private $email;

	
	// Getters and setters
	/**
	 * @return the name
	 */
	public function getName() 
	{
		return $this->name;
	}

	/**
	 * @return the email
	 */
	public function getEmail()
	{
		return $this->email;
	}

	
	/**
	 * @param the name
	 */
	public function setName($name) 
	{
		$this->name = $name;
	}	
	
	/**
	 * @param the email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}

}
