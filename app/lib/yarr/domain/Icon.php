<?php
namespace yarr\domain;

class Icon
{
	// Object property fields.	
	private $mimeType;
	private $iconImage;
	private $size;
	
	// Getters and setters
	/**
	 * @return the mimeType
	 */
	public function getMimeType() 
	{
		return $this->mimeType;
	}

	/**
	 * @return the iconImage
	 */
	public function getIconImage()
	{
		return $this->iconImage;
	}
	
	/**
	 * @return the size
	 */
	public function getSize()
	{
		return $this->size;
	}

	/**
	 * @param the mimeType
	 */
	public function setMimeType($mimeType) 
	{
		$this->mimeType = $mimeType;
	}	
	
	/**
	 * @param the iconImage
	 */
	public function setIconImage($iconImage)
	{
		$this->iconImage = $iconImage;
	}
	
	/**
	 * @param the size
	 */
	public function setSize($size)
	{
		$this->size = $size;
	}
}
