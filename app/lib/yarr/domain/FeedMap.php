<?php
namespace yarr\domain;

class FeedMap 
{
	private $id;
	private $remoteId;
	// Whatever other field is needed.

	/**
	 * @return the $id
	 */
	public function getId() 
	{
		return $this->id;
	}

	/**
	 * @param field_type $id
	 */
	public function setId($id) 
	{
		$this->id = $id;
	}

	/**
	 * @return the $remote_id
	 */
	public function getRemoteId() 
	{
		return $this->remoteId;
	}

	/**
	 * @param field_type $Feedname
	 */
	public function setRemoteId($remoteId) 
	{
		$this->remoteId = $remoteId;
	}	
}
