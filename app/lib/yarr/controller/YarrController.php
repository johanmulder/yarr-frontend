<?php

namespace yarr\controller;

use yarr\Database;
use yarr\session\SessionHandler;
use yarr\dao\impl\DAOFacadeImpl;
use yarr\dao\DAOFacade;
use yarr\dao\FeedDAO;
use yarr\domain\FeedSubscription;
use yarr\domain\FeedMap;
use yarr\domain\FeedItem;

/**
 * The Yarr main controller class. Contains the action functions which are
 * of the reader, executed via the action string in the browser. 
 * @author Vincent Vogelesang
 */
class YarrController
{
	/**
	 * Database object.
	 * @var db
	 */
	private $db = null;
	private $DAOFacade = null;
	private $session = null;
	private $feedDAO = null;
	private $userDAO = null;
	private $feedMapDAO = null;
	private $feedSubscriptionDAO = null;
	private $maxPageItems = 99999999;
	private $itemOffset = 0;
	private $template_data = null;
	private $feedSelection = null;
	private $searchSelection = null;
	
	/**
	 * Class constructor.
	 * @param PDO $pdo
	 */
	public function __construct(Database $db, DAOFacade $DAOFacade, SessionHandler $session)
	{
		$this->db = $db;
		$this->session = $session;
		$this->DAOFacade = $DAOFacade;
		$this->feedDAO = $this->DAOFacade->GetFeedDAO();
		$this->userDAO = $this->DAOFacade->GetUserDAO();
		$this->feedMapDAO = $this->DAOFacade->GetFeedMapDAO();
		$this->feedSubscriptionDAO = $this->DAOFacade->GetFeedSubscriptionDAO();		
	}

	/**
	 * Home action, shows the login screen
	 * @return array
	 */
	public function actionHome()
	{
		$timeStamp = microtime(TRUE); // Used to calculate fetch time
		
		// Get the feeds for the user as array
		$feedArray = $this->getFeedArray(); 		
		
		// Get all the feed items by feedId, check for Feed selection
		$feedArraySelection = $feedArray; // Make selection copy
		if ($this->feedSelection !== null) {
			$feedArraySelection = array($this->feedSelection);	// change array to selected feed Id	ONLY	
		}
		
		// Check if there is a search selection of feed items
		if ($this->searchSelection !== null) {
			$feedItemArray = $this->searchSelection;
		}
		
		// If no search selection present get the feedItems
		else {
			$feedItemArray = array();
			$items = array();
			foreach ($feedArraySelection as $feedId)
			{
				$items = $this->feedDAO->getFeedItems($feedId, $this->maxPageItems, $this->itemOffset); // return all the items for the feedId
				$result = array_merge($feedItemArray, $items); // merge all individual feed arrays into on big array for sorting.
				$feedItemArray = $result;
				usort($feedItemArray, array($this, "compare"));
			}
			
		}
				
		// Push feed & item array data in to template_data array
		$this->template_data['feedArray']= $feedArray;
		$this->template_data['feedItemArray']= $feedItemArray;
		$this->template_data['DAOFacade'] = $this->DAOFacade;	
		$this->template_data['maxPageItems'] = $this->maxPageItems;
		$this->template_data['maxPageItems'] = $this->maxPageItems;
		$this->template_data['timeStamp'] = $timeStamp;
		return $this->template_data;
	}

	/**
	 * Delete subscriptions action
	 * @return array
	 */
	public function actionDeleteSubscriptions()
	{
		if (isset($_POST['deleteSubscriptions'])) 
		{
			foreach ($_POST['deleteSubscriptions'] as $subscriptionId)
			{
				$feedSubscription = new FeedSubscription();
				$feedSubscription->setId($subscriptionId);
				$this->feedSubscriptionDAO->delete($feedSubscription);
			}		
		}
		return $this->actionHome();
	}
	
	/**
	 * Change password update action
	 * @return array
	 */
	public function actionChangePassword()
	{
		//Changepass check
		$changepasshandler = new \yarr\security\ChangePassHandlerImpl($this->userDAO);
		try
		{
			$changePassStatus = $changepasshandler->changePass($_POST['password'],$_POST['password2']);
			//If it's allowed set the new pass for the current user.
			if ($changePassStatus)
			{
				$user = $this->userDAO->getUserByEmail($_SESSION['securityticket']->getUsername());
				$user->setPassword($_POST['password']);
				$this->userDAO->update($user);
			}
		}
		//If it wasn't allowed we set template_data with the error.
		catch (\Exception $e)
		{
			$this->template_data['errormsg'] = $e->getMessage();
		}
		//No exception then we can set template_data with succes
		$this->template_data['successful'] = "Password changed successfully !";
		return $this->actionHome();
	}
		
	/**
	 * Add RSS action
	 * @return array
	 */
	public function actionAddRss() // changed 10-4
	{
		// Get the user id from email addresss		
		$email = $_SESSION['securityticket']->getUsername();
		$userId = $this->userDAO->getUserByEmail($email)->getId();
			
		// Add the feed to MongoDB and get it's Remote feedID + title		
		$remoteId = $this->feedDAO->addFeed($_POST['rssUrl']);
		if ($remoteId !== null) 
		{
			$name = $this->feedDAO->getFeed($remoteId)->getTitle(); // used for feedsubscription		
			
			// Check if feed exists already
			$subscriptionArray = $this->feedSubscriptionDAO->getFeedSubscriptionByUserId($userId);
			$subscriptionExists = false; 
			foreach ($subscriptionArray as $object) 
			{
				if ($object->feed_id === $this->getFeedMapId($remoteId)) 
				{
					$subscriptionExists = true;
				}								
			}  
					
			if (!$subscriptionExists)
			{
		 		// Add the feed to MySQL Feed(Map) table  		
		 		$this->addFeedMapItem($remoteId);
				
				// Get the MySQL feedId		
				$feedId = $this->getFeedMapId($remoteId);	
						
				// Add both id's to the FeedSubscription table
				$this->addSubscription($userId, $feedId, '1', $name);									
			}
		}
		return $this->actionHome();
	}
	
	/**
	 * Search action
	 * @return array
	 */
	public function actionSearch()
	{
		$this->searchSelection = $this->getSearchSelection();
		return $this->actionHome();		
	}
	
	/**
	 * FeedSelect action
	 * @return array
	 */
	public function actionFeedSelect()
	{
		$this->feedSelection = $this->getCurrentFeedSelection();		
		return $this->actionHome();
	}
	
	/**
	 * Gets the Mysql Id of a feed
	 *
	 * @param string $remoteId
	 * @return string feed(Map)Id;
	 */
	private function getFeedMapId($remoteId)
	{
		$feedMap = $this->feedMapDAO->getFeedByRemoteId($remoteId);
		if (!empty($feedMap)) return $feedMap[0]->feed_id;
		else return null;
	}
	
	/**
	 * Add a feed to MySQL Feed(Map) table
	 * 
	 * @param string the remoteId
	 */
	private function addFeedMapItem($remoteId)
	{
		$feedMap = new FeedMap();
		$feedMap->setRemoteId($remoteId);
		$this->feedMapDAO->create($feedMap);
	}
	
	/**
	 * Add a subscription to MySQL feed_subscription table
	 * 
	 * @param string $userId
	 * @param string $feedId
	 * @param string $categoryId
	 * @param string $name
	 */	
	private function addSubscription($userId, $feedId, $categoryId, $name)
	{
		$feedSubscription = new FeedSubscription();
		$feedSubscription->setUserId($userId);
		$feedSubscription->setFeedId($feedId);
		$feedSubscription->setCategoryId($categoryId); // Default category 'All'
		$feedSubscription->setName($name);
		$this->feedSubscriptionDAO->create($feedSubscription);
	}
	
	/**
	 * Get the current feed selection
	 * 
	 * @return string feed title
	 */
	private function getCurrentFeedSelection()
	{
		$selection = null;
		if (isset($_POST['feedSelection']) == true && $_POST['feedSelection'] !=='showAll'){
			$selection = $_POST['feedSelection'];
		}		
		return $selection;		
	}

	/**
	 * Sort function for usort. Used to sort
	 * an array with feed items by pubdate
	 *
	 * @param feeditem A
	 * @param feeditem B
	 * @return boolean
	 *
	 */
	public function compare(FeedItem $a, FeedItem $b)
	{
		$time1 = strtotime($a->getPubDate());
		$time2 = strtotime($b->getPubDate());
		if ($time2 > $time1)
			return 1;
		elseif ($time2 == $time1)
		return 0;
		return -1;
	}
	
	/**
	 * Get the feeds for the user
	 * @return Array with (long) feedId's
	 */
	private function getFeedArray()
	{
		// Get the feeds for the user as array
		$user = $this->userDAO->getUserByEmail($_SESSION['securityticket']->getUsername());
		$userId = $user->getId();
		$feedSubscriptionArray = $this->feedSubscriptionDAO->getFeedSubscriptionByUserId($userId);
		
		// Remap all MySQL feedId's to MongoDB feedId's
		$feedArray = array();
		foreach ($feedSubscriptionArray as $object)
		{
			$feedMapItem = $this->feedMapDAO->getFeedById($object->feed_id);
			array_push($feedArray, $feedMapItem[0]->remote_id);
		}
		return $feedArray;
	}
	
	/**
	 * Get the search selection
	 *
	 * @return array with FeedItem search results
	 */
	private function getSearchSelection()
	{
		$searchResult = null;
		if (isset($_POST['searchString']) == true && $_POST['searchString'] !=='') {
			$searchResult = $this->feedDAO->getSearchItems($_POST['searchString'], $this->getFeedArray());
		}
		return $searchResult;
	}
}
