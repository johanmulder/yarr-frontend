<?php

namespace yarr\controller;

use yarr\Database;
use yarr\session\SessionHandler;
use yarr\dao\impl\DAOFacadeImpl;
use yarr\dao\DAOFacade;
use yarr\security\UserNotFoundException;

/**
 * The controller class. Contains the action functions which are
 * executed via the action string in the browser. 
 * @author Johan Mulder <johan@mulder.net> & Vincent Vogelesang
 */
class LoginController
{
	/**
	 * Database object.
	 * @var db
	 */
	private $db = null;
	private $DAOFacade = null;
	private $session = null;

	/**
	 * Class constructor.
	 * @param PDO $pdo
	 */
	public function __construct(Database $db, DAOFacade $DAOFacade, SessionHandler $session)
	{
		$this->db = $db;
		$this->DAOFacade = $DAOFacade;
		$this->session = $session;		
	}

	/**
	 * Home action, shows the login screen
	 * @return array
	 */
	public function actionHome()
	{
		//Clear old securitytickets
		$this->session->clear("securityticket");
		$template_data = array();
		$template_data['whoJustRegistered'] = $this->session->get("whoJustRegistered");	
		$template_data['email'] = $this->session->get("whoJustRegistered");	
		
		return $template_data;
	}

	/**
	 * Checks login credentials
	 * @return template_data
	 */	
	public function actionLogin() 
	{
		$this->session->clear("whoJustRegistered");//If there was a key/value we delete it.
		
		$template_data = array();
		
		
		//Login check
		$userDAO = $this->DAOFacade->GetUserDAO();
		$loginhandler = new \yarr\security\LoginHandlerImpl($userDAO);		
		try
		{
			$securityticket = $loginhandler->authenticate($_POST['email'],$_POST['password']);
		
			// Check if securityticket contains information. Set and save object securityticket in session and invoke yarr
			if (!is_null($securityticket))
			{		
				$this->session->set("securityticket", $securityticket);
				header("Location: /index.php/yarr"); // redirect to yarr page via index.php
				exit;
			}		
		}
		catch (\Exception $e)
		{
			$template_data['email'] = $_POST['email']; //Keep the email address.
			$template_data['errormsg'] = $e->getMessage();
		}
		return $template_data;
	}	
	
	/**
	 * Go to register page
	 * @return array
	 */
	public function actionRegister()
	{	
		return array();
	}
}
