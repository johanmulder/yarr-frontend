<?php

namespace yarr\controller;

use yarr\Database;
use yarr\session\SessionHandler;
use yarr\dao\impl\DAOFacadeImpl;
use yarr\dao\DAOFacade;
use yarr\domain\User;

/**
 * The RegisterController class. Contains the action functions which are
 * executed via the action string in the browser. 
 * @author Jan Hoogstins & Vincent Vogelesang
 */
class RegisterController

{
	/**
	 * Database object.
	 * @var db
	 */
	private $db = null;
	private $DAOFacade = null;
	private $session = null;

	/**
	 * Class constructor.
	 * @param PDO $pdo
	 */
	public function __construct(Database $db, DAOFacade $DAOFacade, SessionHandler $session)
	{
		$this->db = $db;
		$this->DAOFacade = $DAOFacade;
		$this->session = $session;		
	}

	/**
	 * Home action, shows the Register screen
	 * @return array
	 */
	public function actionHome()
	{
		// No template data required.
		return array();
	}

	/**
	 * Try to register a user
	 * @return template_data
	 */	
	public function actionRegister() 
	{
		$template_data = array();

		//Register check
		$userDAO = $this->DAOFacade->GetUserDAO();
		$registerhandler = new \yarr\security\RegisterHandlerImpl($userDAO);
		
		try
		{
			$regStatus = $registerhandler->registerUser($_POST['email'],$_POST['password'],$_POST['password2']);
		
			//If user information is valid , make user and go to login page.
			if ($regStatus)
			{		
				$user = new User();
				$user->setEmail($_POST['email']);
				$user->setPassword($_POST['password']);
				$user->setStatus('enabled');
				$userDAO->create($user);
				$this->session->set("whoJustRegistered",$_POST['email']); // Make a little key with value in the session, so next page gives a small notification if it detects and knows who it was.
				header("Location: /index.php/login"); // redirect to login page
				exit;
			}		
		}
		catch (\Exception $e)
		{
			$template_data['email'] = $_POST['email']; //Keep the email address.
			$template_data['errormsg'] = $e->getMessage();
		}
		return $template_data;
	}
		
}
