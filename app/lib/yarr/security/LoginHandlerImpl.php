<?php

namespace yarr\security;

/**
 * Class handling user logins.
 * @author JH
 */
Class LoginHandlerImpl implements LoginHandler 
{
	private $userDAO = null;
	
	public function __construct($userDAO)
	{
		$this->userDAO = $userDAO;
	}
		
	/**
	 * Attempt to authenticate a user with a given password
	 * @param string $username
	 * @param string $password
	 * @return SecurityTicketImpl The security context if successful.
	 * @throws PasswordIncorrectException Thrown if the password is incorrect.
	 * @throws UserNotFoundException Thrown if the user doesn't exist.
	 * @throws AuthenticationFailedException Optional.
	 */
	public function authenticate($username, $password)
	{
		$user = $this->userDAO->getUserByEmail($username);
			
		//Try to authenticate
		//Check if the username/email is known/valid
		if ($username == "")
			throw new \Exception ('Please enter a email address !');
			
		if(!filter_var($username, FILTER_VALIDATE_EMAIL))
			throw new \Exception ('That\'s not a valid email address !');	
			
		if (is_null($user) || is_null($user->getEmail()))
			throw new UserNotFoundException('User not found!');
		//Check if password equals.
		if($user->getPassword() !== $password){
			throw new PasswordIncorrectException('Password incorrect!');
		}
		//Check if status is enabled.
		if($user->getStatus() !== "enabled"){
			throw new UserStatusException('User disabled!');
		}
			
		//Authentication succeed, make a securityticket for the user.(For storing in session)
		return new SecurityTicketImpl($user);
	}
	
	
}
