<?php

namespace yarr\security;

class AuthenticationFailedException extends \Exception 
{
  public function errorMessage()
  {
     //Error message
     $errorMsg = '</b> Authentication not succeed';
     return $errorMsg;
  }
}
