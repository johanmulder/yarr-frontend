<?php

namespace yarr\security;

class SecurityTicketImpl implements SecurityTicket
{
	public function __construct($user)
	{
		$this->user = $user;
	}
	
	/**
	 * Get the username/email of the security context.
	 * @return
	 */
	public function getUsername(){
		return $this->user->getEmail();
	}
	
	/**
	 * Get the password in the security context.
	 * @return
	 */
	public function getPassword(){
	
	}
	
	/**
	 * Get user entry
	 * @return
	 */
	public function getStatus(){
	
	}
}

?>