<?php

namespace yarr\security;

/**
 * Interface for a class handling user logins.
 * @author Johan Mulder <johan@mulder.net>
 */
interface LoginHandler 
{
	/**
	 * Attempt to authenticate a user with a given password
	 * @param string $username
	 * @param string $password
	 * @return SecurityTicketImpl The security context if successful.
	 * @throws PasswordIncorrectException Thrown if the password is incorrect.
	 * @throws UserNotFoundException Thrown if the user doesn't exist.
	 * @throws AuthenticationFailedException Optional.
	 */
	public function authenticate($username, $password);

}
