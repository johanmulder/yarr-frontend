<?php

namespace yarr\security;

class UserStatusException extends \Exception 
{
  public function errorMessage($status)
  {
	if ($status == "disabled"){
		$errorMsg = '</b> Account is disabled!';
	}
	elseif ($status == "ended"){
		$errorMsg = '</b> Account has ended!';
	}
	else{
		$errorMsg = '</b> Account has no status,that\'s not good....';
	}

     return $errorMsg;
  }
}
