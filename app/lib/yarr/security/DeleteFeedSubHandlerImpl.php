<?php

namespace yarr\security;

/**
 * Class handling DeleteFeedSub.
 * @author JH
 */
Class DeleteFeedSubHandlerImpl implements DeleteFeedSubHandler 
{
	private $userDAO = null;
	
	public function __construct($userDAO)
	{
		$this->userDAO = $userDAO;
	}
		
	/**
	 * Try to check if feedsubscription is valid and return the status. 
	 * @return boolean True if feedsubscription may be deleted.
	 */
	public function deleteFeedSub($feedsub);
		
		//Check if the feedsub is valid.
		if ($password == "")
			throw new \Exception ('You didn\'t gave a feedsubscription !');

		//Check if password isnt the same.
		$user = $this->userDAO->getUserByEmail($_SESSION['securityticket']->getUsername());	
		if ($user->getPassword() === $password)		
			throw new \Exception('Can\'t change a password that is the same');
		
		//All checks passed so we return true.
		return true;

	}
	
	
}
