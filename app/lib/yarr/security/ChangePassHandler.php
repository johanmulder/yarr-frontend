<?php

namespace yarr\security;

/**
 * Interface for a class changepass handling.
 * @author JH
 */
interface ChangePassHandler
{
	/**
	 * Attempt change the users password with the given passwords
	 */
	public function changePass($password,$password2);

}
