<?php

namespace yarr\security;

class PasswordIncorrectException extends \Exception 
{
  public function errorMessage()
  {
     //Error message
     $errorMsg = '</b> Password is niet correct';
     return $errorMsg;
  }
}
