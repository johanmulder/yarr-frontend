<?php

namespace yarr\security;

/**
 * Class handling changepass.
 * @author JH
 */
Class ChangePassHandlerImpl implements ChangePassHandler 
{
	private $userDAO = null;
	
	public function __construct($userDAO)
	{
		$this->userDAO = $userDAO;
	}
		
	/**
	 * Try to check if the password is valid and return the status. 
	 * @return boolean True if password are valid.
	 */
	public function changePass($password,$password2)
	{
		
		
		//Check if the passwords are valid.
		if ($password == "")
			throw new \Exception ('You didn\'t gave a password !');
		if ($password2 !== $password )
			throw new \Exception('The passwords you gave weren\'t equal, please repeat the same password');
		//Check if password isnt the same.
		$user = $this->userDAO->getUserByEmail($_SESSION['securityticket']->getUsername());	
		if ($user->getPassword() === $password)		
			throw new \Exception('Can\'t change a password that is the same');
		
		//All checks passed so we return true.
		return true;

	}
	
	
}
