<?php

namespace yarr\security;

/**
 * Class handling register users.
 * @author JH
 */
Class RegisterHandlerImpl implements RegisterHandler 
{
	private $userDAO = null;
	
	public function __construct($userDAO)
	{
		$this->userDAO = $userDAO;
	}
		
	/**
	 * Try to register a user, 
	 * @return boolean True if registration succeeds.
	 */
	public function registerUser($username, $password, $password2)
	{
		//Try to register
		
		//Check if the username is valid.
		if ($username == "")
			throw new \Exception ('Please enter a email address !');
			
		if(!filter_var($username, FILTER_VALIDATE_EMAIL))
			throw new \Exception ('That\'s not a valid email address !');
		
		$user = $this->userDAO->getUserByEmail($username);
		//Check if username/ email adress is already known in db system.
		if ($user->getEmail() === $username)		
			throw new \Exception('Email address already exists, please fill in a other email address !');
			
		if ($password == "")
			throw new \Exception('Please enter a password !');
			
		if ($password2 !== $password )
			throw new \Exception('The passwords aren\'t equal, please repeat the same password');
			
		
		return true;

	}
	
	
}
