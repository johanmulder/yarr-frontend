<?php

namespace yarr\security;

/**
 * Interface for a class register handling.
 * @author JH
 */
interface RegisterHandler
{
	/**
	 * Attempt to register a user with the given passwords
	 */
	public function registerUser($username, $password, $password2);

}
