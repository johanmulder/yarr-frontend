<?php

namespace yarr\security;

/**
 * Interface for a class DeleteFeedSub handling.
 * @author JH
 */
interface DeleteFeedSubHandler
{
	/**
	 * Return a boolean if feedsubscription may be deleted.
	 */
	public function deleteFeedSub($feedsub);

}
