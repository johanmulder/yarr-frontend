<?php

namespace yarr\security;

/**
 * Interface for a class SecurityTicket.
 * @author JH
 */
interface SecurityTicket 
{
	/**
	 * Get the username/email of the security context.
	 * @return
	 */
	public function getUsername();
	
	/**
	 * Get the password in the security context.
	 * @return
	 */
	public function getPassword();
	
	/**
	 * Get user entry
	 * @return
	 */
	public function getStatus();

}


?>