<?php
namespace yarr\security;

class UserNotFoundException extends \Exception 
{

  public function errorMessage()
  {
     //Error message
     $errorMsg = '</b> User not found!';
	 
     return $errorMsg;
  }
   
}
