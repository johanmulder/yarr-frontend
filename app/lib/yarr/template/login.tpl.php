<?php
	//Include the html header
	new yarr\template\Htmlhead("Login");
	
?>
			
		<div class="container">   
<?php if (array_key_exists('whoJustRegistered', $template_data) && $template_data['whoJustRegistered'] !==  null ): ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				 <?= "Yarr !!! The registration for " . $template_data['whoJustRegistered'] . " has been successful. You can now login."?> 
			</div>
<?php endif; ?>	
<?php if (array_key_exists('errormsg', $template_data)): ?>
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?= $template_data['errormsg'] ?>
			</div>
<?php endif; ?>
			<div class="span4 offset4">
				<br/>
				<img src="/img/logo_new.jpg" alt="Yarr dude" /><br/><br/>
				<div id="legend">			    		
					<legend class="">Login</legend>
				</div>
				<form action="/index.php/login" method="POST">
					<label>Email address</label>
					<input type="email" name="email" class="span3" value= "<?php if (array_key_exists('email', $template_data)) echo $template_data['email'] ;	?>" >
					<label>Password</label>
					<input type="password" name="password" class="span3">
					<div>
						<input type="submit" value="Login!" class="btn pull-left">
						<input type="hidden" name="action" value="login">
					</div>
					<br/>
					<div class="clearfix"></div>				
				</form>
				<form name="register" action="/index.php/register" method="POST">
					<label>Don't have a login yet? Register
					<a href="javascript: document.register.submit()">here</a>
					</label>					
				</form>
			</div>
		</div>
	</body>
</html>
			
