<?php
namespace yarr\template;

/**
 * This class echo the html header with a specific title.
 */
class Htmlhead
{
	/**
	 * Class constructor.
	 */
	public function __construct($title)
	{
		echo '<!DOCTYPE html>
				<html>
					<head>
						<title>'.$title.'</title>
						<meta name="viewport" content="width=device-width, initial-scale=1.0">
						<link href="/css/layout.css" rel="stylesheet" type="text/css" />
						<link href="/css/bootstrap.css" rel="stylesheet" media="screen" />
					</head>
					<body>
						<script src="http://code.jquery.com/jquery.js"></script>
						<script src="/js/bootstrap.js"></script>';
	}
}
							
	