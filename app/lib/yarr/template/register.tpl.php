<?php
	//Include the html header
	new yarr\template\Htmlhead("Register");
?>
		
		<div class="container">    
<?php if (array_key_exists('errormsg', $template_data)): ?>
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?= $template_data['errormsg'] ?>
			</div>
<?php endif; ?>   
			<div class="span4 offset4">
				<br/>
				<img src="/img/logo_new.jpg" alt="Yarr dude" /><br/><br/>
				<div id="legend">			    		
					<legend class="">Register at YARR</legend>
				</div>
				<form action="" method="POST">
					<label>Email address</label>
					<input type="email" name="email" class="span3" value= "<?php if (array_key_exists('email', $template_data)) echo $template_data['email'] ;	?>" >
					<label>Password</label>
					<input type="password" name="password" class="span3">
					<div>
					<label>Repeat password</label>
					<input type="password" name="password2" class="span3">
					<div>
						<input type="submit" value="Register!" class="btn pull-left">
						<input type="hidden" name="action" value="register">
					</div>				
					<div class="clearfix"></div>
				</form>
				<form name="login" action="/index.php/login" method="POST"><br/>
					<label>You want to login? Login
					<a href="javascript: document.login.submit()">here</a>
					</label>					
				</form>
			</div>
		</div>
	</body>
</html>
			
