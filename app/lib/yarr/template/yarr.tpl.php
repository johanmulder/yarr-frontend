<?php
	//Include the html header
	new yarr\template\Htmlhead("Yarr");
	
	// Get all the DAO items and arrays
	$feedArray = $template_data['feedArray'];
	$feedItemArray = $template_data['feedItemArray'];
	$maxPageItems = $template_data['maxPageItems'];	
	$DAOFacade = $template_data['DAOFacade'];
	$feedDAO = $DAOFacade->GetFeedDAO();
	$userDAO = $DAOFacade->GetUserDAO();
	$feedSubscriptionDAO = $DAOFacade->GetFeedSubscriptionDAO();
	
?>
		<div id="header">
			<div id="headerlogo">
				<table>
			    	<tr>    							
			    		<td><img src="/img/logo_new.jpg" alt="Yarr dude" /></td>
						<td width="100%" VALIGN="top">
							<?php if (array_key_exists('errormsg', $template_data)): ?>
								<div class="alert alert-error">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
										<?= $template_data['errormsg'] ?>
								</div>
								<?php endif; ?>   
								<?php if (array_key_exists('successful', $template_data) && !array_key_exists('errormsg', $template_data) ): ?>
								<div class="alert alert-success">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<?= $template_data['successful'] ?>
								</div>
								<?php endif; ?>  
							</td>
						</tr>
				</table>
			</div>		
		
			<div id="settings" class="modal hide fade">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3>Settings</h3>					
				</div>
				<div class="modal-body">
    				<form name="settings" action="/index.php/yarr" method="POST">
    					<h4>Delete subscriptions</h4>
    					<table id="subscriptions" class="table">
    						<th>Feed name</th><th>Delete</th>
    						<?php 
    							$user = $userDAO->getUserByEmail($_SESSION['securityticket']->getUsername());
    							$userId = $user->getId();
    							$subscriptionArray = $feedSubscriptionDAO->getFeedSubscriptionByUserId($userId);
    							foreach ($subscriptionArray as $object)
    							{
    								echo'<tr>
											<td>' . $object->name . '</td>
											<td><input type="checkbox" name="deleteSubscriptions[]" value="' . $object->feed_subscription_id . '"></td>
										 </tr>';
    							}	    						
		    				?>
    					</table>
    					<br/>    				
    					<input type="hidden" name="action" value="DeleteSubscriptions">
    				</form>   				
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
					<a href="javascript: document.settings.submit()" class="btn btn-primary" >Save changes</a>
				</div>
			</div>
			
			<div id="changepassword" class="modal hide fade">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3>Change password</h3>
				</div>
				<div class="modal-body">
    				<form name="changepassword" action="/index.php/yarr" method="POST">
    					<table id="changepassword" class="table">
    						<th>Change Password</th><th></th><th></th>
    						<tr>    							
    							<td>New password</td>
    							<td><input type="password" name="password" class="span3"></td>
    						</tr>
    						<tr>    							
    							<td>Repeat new password</td>
    							<td><input type="password" name="password2" class="span3"></td>
    						</tr>
    					</table>
    					<input type="hidden" name="action" value="changepassword">
    				</form>   				
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
					<a href="javascript: document.changepassword.submit()" class="btn btn-primary" >Save new password</a>
				</div>
			</div>		
			
			<div id="loginstate">
				<form name="logout" action="/index.php" method="POST"></form>
						
			</div>			
		</div>	
				
		<div id="navibar">
			<div class="navbar">
				<div class="navbar-inner">									
					<ul class="nav pull-right">
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-user"></i>
							<?php print_r($_SESSION['securityticket']->getUsername());?><b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a data-toggle="modal" data-target="#settings">Settings</a></li>
								<li><a data-toggle="modal" data-target="#changepassword">Change password</a></li>
								<li><a href="javascript: document.logout.submit()">Logout</a></li>
							</ul>											
						</li>	
					</ul>
					<ul class="nav pull-right">
						<li><a href="javascript: document.search.submit()">Search</a></li>		
					</ul>
					<form class="navbar-search pull-right" name="search" method="POST">
						<input type="text" class="search-query" name="searchString" placeholder="Item">						
						<input type="hidden" name="action" value="search">
					</form>

					<ul class="nav pull-right">
						<li class="divider-vertical"></li>
					</ul>
					
				    <form class="navbar-search pull-left" name="addRss" method="POST">
				 		<input type="text" class="search-query" name="rssUrl" placeholder=" RSS feed">
				  		<input type="hidden" name="action" value="addRss">
					</form>
					<ul class="nav pull-left">
						<li><a href="javascript: document.addRss.submit()">Add</a></li>		
					</ul>
					
					<ul class="nav pull-left">
						<li class="divider-vertical"></li>						
					</ul>
				</div>
			</div>
		</div>
		
		<div id="menu">
			<form name="showAll" method="POST">								
				<input type="hidden" name="action" value="FeedSelect">
				<input type="hidden" name="feedSelection" value="showAll">
			</form>
			<?php
				$feedCount = count($feedArray);
				$itemId = 1;

				foreach ($feedArray as $feedId)
				{
					$feed = $feedDAO->getFeed($feedId);											
					echo '<form name="link'. $itemId . '" method="POST">								
								<input type="hidden" name="action" value="FeedSelect">
								<input type="hidden" name="feedSelection" value="' . $feedId . '">
						  </form>';							
					$itemId++;
				}

			?>	
			<ul class="nav nav-list">
				<li><label class="tree-toggler nav-header">
					<?php
						
						$feedCount = count($feedArray);
						$itemId = 1;
						echo '<a href="javascript: document.showAll.submit()" style="font-size:110%;">All items (' . $feedCount . ')</a></label>
							  <ul class="nav nav-list tree">';
						foreach ($feedArray as $feedId)
						{
							$feed = $feedDAO->getFeed($feedId);											
							echo '<li>
									<a href="javascript: document.link' . $itemId . '.submit()" style="font-size:90%;">' . $feed->getTitle() . ' (' . $feed->getNumFeedItems() . ')</a>
	    						  </li>';							
							$itemId++;
							
						}
					?>										
				   </ul>					
				</li>														
			</ul>
		</div>

		<div id="content">			
			<script type="text/javascript">
				var currentRow = null;
				function fetchRow(rowId) {
					if (rowId == currentRow) {
						$('#description' + rowId).hide(200);
						$('#item' + rowId).removeClass('active_item');
						currentRow = null;
					} 
					else {
							// Unhide the description row
							$('#description' + rowId).show(200);
							// Mark the row as 'read'.
							$('#item' + rowId).removeClass('unread');
							// Set the row active
							$('#item' + rowId).addClass('active_item');
							// Hide the current row if there is any.
							if (currentRow !== null && currentRow != rowId) {
								$('#description' + currentRow).hide(200);
								$('#item' + currentRow).removeClass('active_item');
							}
							// 
							// Set the current row to the selected row.
							currentRow = rowId;
							
						}
					}		
			</script>				
			<table class="itemtable">			
				<?php				
					
					$feedObjectArray = array();
					$feedItemCount = 0;

					// fetch all the feed objects in one go to save time
					foreach ($feedArray as $feedId)
					{	
						array_push($feedObjectArray, $feedDAO->getFeed($feedId));
					}

					// Print all the items for the user
					foreach ($feedItemArray as $item)
					{
						$feed = $feedObjectArray[array_search($item->getFeedId(), $feedArray)]; // Get the title from Feed object
						$feedItemCount++;
						echo ' 
							<tr id="item' . $item->getId() . '" class="unread" onclick="fetchRow(\'' . $item->getId() . '\');">
								<td>' . $feed->getTitle() . '</td>
								<td>' . $item->getTitle() . '</td>
								<td>' . $item->getPubDate() . '</td>
								<td><a href="javascript:window.external.AddFavorite(\'' . $item->getUrl() . '\',\'' . $item->getTitle() . '\')"><i class="icon-star-empty"></i></a></td>
								<td><a onclick="window.open(this.href,\'_blank\');return false;" href="' . $item->getUrl() . '"><i class="icon-globe"></i></a></td>
							</tr>
							<tr class="hidden" id="description' . $item->getId() . '">
								<td colspan="5">
								<div id="contents' . $item->getId() . '">' . $item->getContents() . '</div>
								</td>
							</tr>';													
					}  
				?>			
			</table>
		</div>
		
		<div id="footer">
			<?php
			$endTime = microtime(TRUE);
			$startTime =  $template_data['timeStamp'];
			$timeTaken = round(($endTime - $startTime),3);			
				echo '<p class="text-right">Total items fetched: ' . $feedItemCount . '&nbsp;&nbsp;&nbsp;&nbsp;Time to fetch: ' . $timeTaken . ' seconds&nbsp;&nbsp;</p>';						  						
			?>		

			<?php
				//mark as reads
				echo '<p class="text-right">Test </p>';					  						
			?>	
			
		</div>		
	</body>
</html>

			


			
