<?php
namespace yarr;

class Configuration 
{
	private static $instance = null;
	
	private function __construct()
	{
		// Initialize configuration here.
		$this->iniConfig = parse_ini_file(__DIR__ . '/config/config.ini', true);
	}
	
	public static function getInstance()
	{
		if (is_null(self::$instance))
			self::$instance = new Configuration();
		return self::$instance;
	}
	
	/**
	 * Get the database username
	 * @return string
	 */
	public function getDbUsername()
	{
		return $this->iniConfig['database']['username'];
	}

	/**
	 * Get the database password
	 * @return string
	 */
	public function getDbPassword()
	{
		return $this->iniConfig['database']['password'];
	}

	/**
	 * Get the database name
	 * @return string
	 */
	public function getDbName()
	{
		return $this->iniConfig['database']['name'];
	}

	/**
	 * Get the database host
	 * @return string
	 */
	public function getDbHost()
	{
		return $this->iniConfig['database']['host'];
	}
	
	/**
	 * Get the REST database url
	 * @return string
	 */
	public function getRestUrl()
	{
		return $this->iniConfig['rest']['url'];
	}


	/**
	 * Get the default action in case an invalid or no action is given.
	 * @return string
	 */
	public function getDefaultAction()
	{
		return $this->iniConfig['actions']['default_action'];
	}
	
	/**
	 * Get the default controller case an invalid or no controller is given.
	 * @return string
	 */
	public function getDefaultController()
	{
		return $this->iniConfig['controllers']['default_controller'];
	}
}
