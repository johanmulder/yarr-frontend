<?php
namespace yarr;

class Database extends \PDO
{
	/**
	 * PDO database object.
	 * @var PDO
	 */
	private $pdo = null;

	public function __construct(Configuration $config)
	{
		$dsn = 'mysql:dbname=' . $config->getDbName() . ';host=' .
				$config->getDbHost();
		parent::__construct($dsn, $config->getDbUsername(), $config->getDbPassword());
	}
}

