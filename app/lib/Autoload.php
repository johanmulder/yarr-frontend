<?php

/**
 * Simple autoloader class.
 * @author Johan Mulder <johan@mulder.net>
 */
class Autoload
{
	/**
	 * The function which does the actual auto loading.
	 * @param string $classname
	 */
	public static function loadClass($classname)
	{
		$path_prefix = '';
		// Check if this is a namespaced class. If so,
		if (strstr($classname, '\\'))
			$classpath = str_replace('\\', DIRECTORY_SEPARATOR, $classname) . '.php';
		else
			$classpath = $classname . '.php';
		$classpath = __DIR__ . DIRECTORY_SEPARATOR . $classpath;
		// Load the classname.
		if (file_exists($classpath)) require_once($classpath);
	}
}
