<?php 

use yarr\Configuration;
use yarr\Application;

// Register the auto loader.
require_once(__DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'Autoload.php');
spl_autoload_register('Autoload::loadClass');

// Run the application.
$app = new Application(Configuration::getInstance());
$app->run();
